# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils.timezone import now

class Base(models.Model):
    uuid = models.UUIDField(unique=True)
    created = models.DateTimeField(null=True, auto_now_add=True)
    updated = models.DateTimeField(null=True, auto_now=True)
    deleted = models.BooleanField(null=True, default=False)

    class Meta:
       abstract = True 


class Zone(Base):
    """
    Represents a Ventilation Module. One Residence can have 3 to 5 zones.
    A module defines a `Zone` in a Residence, for which a desired temperature
    and ventilation setting is set.
    """

    # Relations
    residence = models.ForeignKey('Residence', on_delete=models.CASCADE, null=True, related_name='+')
    users = models.ManyToManyField('User', related_name='+')
    
    name = models.CharField(max_length=255,  null=True)
    internal_name = models.CharField(unique=True, max_length=255,  null=True)
    
    # TODO: convert to Float of Int (in code /10)?
    desired_temperature = models.FloatField( null=True)
    desired_temperature_updated = models.DateTimeField( null=True)
    
    ventilation_auto_mode = models.BooleanField( null=True, default=True)
    ventilation_manual_position = models.IntegerField( null=True, default=0)
    ventilation_manual_position_adjusted = models.DateTimeField( null=True, default=now)
    ventilation_reset_to_auto_mode_duration = models.IntegerField(null=True, default=30)
    ventilation_reset_to_auto_mode_time = models.DateTimeField(blank=True, null=True)
    
    # TODO: add 1 point float returns
    area = models.FloatField(null=True)

    class Meta:
        db_table = 'zones'


class Residence(Base):
    name = models.CharField(unique=True, max_length=255)
    internal_name = models.CharField(unique=True, max_length=255)
    
    address_street = models.CharField(max_length=255)
    address_number = models.CharField(max_length=255)
    address_pc = models.CharField(max_length=255)
    address_city = models.CharField(max_length=255)
    
    # PVs (solar panel)
    pv_orientation = models.IntegerField( null=True)
    
    weather_station_id = models.CharField(unique=True, max_length=255,  null=True)
    weather_station_api_key = models.CharField(unique=True, max_length=255,  null=True)

    # Relations
    zones = models.ManyToManyField('Zone', related_name='+')
    users = models.ForeignKey('User', on_delete=models.CASCADE, null=True, related_name='+')


    def __str__(self):
        return f"Residence {self.id} ({self.name})"

    class Meta:
        db_table = 'residences'


class User(Base):
    """
    Represents something or someone with access to the backend.
    Every residence has 2 users:
      1) Frontend user
      2) Embedded user

    A User has a one-to-many relation with a Residence (one residence has two users).
    A User has a many-to-many relation with a Zone
     (most of the time a User will have 3 zones, every Zone has two aforementioned Users)

    Admins don't have any relation to Residence nor Zone
    """
    identifier = models.CharField(unique=True, max_length=255)
    secret = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    refresh_token = models.CharField(max_length=255, null=True)
    refresh_token_valid = models.BooleanField(null=True, default=True)
    blocked = models.BooleanField(null=True, default=False)
    phone = models.CharField(max_length=255, null=True)
    
    # Relations
    residence = models.ForeignKey('Residence', on_delete=models.CASCADE, null=True, related_name='+')
    zones = models.ManyToManyField('Zone', related_name='+')

    def __str__(self):
        return f"User with ID {self.id} for Residence " \
               f"`{self.residence.name if self.residence.name else self.residence_id}`"

    class Meta:
        db_table = 'users'


class PasswordResetToken(Base):
    email = models.CharField(unique=True, max_length=255)
    token = models.CharField(max_length=255)

    class Meta:
        db_table = 'password_reset_tokens'


# class UserZone(Base):
#     user = models.ForeignKey('User', on_delete=models.CASCADE, blank=True, null=True, related_name='+')
#     zone = models.ForeignKey('Zone', on_delete=models.CASCADE, blank=True, null=True, related_name='+')

#     class Meta:
#         db_table = 'users_zones'
