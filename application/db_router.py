# DB router for application

class ApplicationDBRouter(object):
    """
    A router to control all database operations on models in the
    application app.
    """
    route_app_labels = {'application'}

    def db_for_read(self, model, **hints):
        """
        Attempts to read application models go to application_db.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'application_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write application models go to application_db.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'application_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the application app is
        involved.
        """
        if (
            obj1._meta.app_label in self.route_app_labels or
            obj2._meta.app_label in self.route_app_labels
        ):
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the application app only appear in the
        'application_db' database.
        """
        if app_label in self.route_app_labels:
            return db == 'application_db'
        return None
